let debounceTimer;
const delay = 500; // Délai en millisecondes (0,5 seconde)
const badWords = ["grosmot1", "grosmot2", "insulte1"]; // Liste de mots à filtrer
const apiKey = 'ryNgzYgV6rj7wnu6EBKkLxEjKIXuZehdxXAGvfqwVAxtH0rZ0amb7Ob2'; // Clé API Pexels
let currentImageIndex = 0;
let images = [];

function handleInput() {
    clearTimeout(debounceTimer);
    debounceTimer = setTimeout(translateWord, delay);
}

function translateWord() {
    const word = document.getElementById('wordInput').value.trim().toLowerCase();

    if (word === "") {
        document.getElementById('translationOutput').textContent = "...";
        document.getElementById('imageOutput').style.display = "none";
        document.getElementById('prevImage').disabled = true;
        document.getElementById('nextImage').disabled = true;
        document.getElementById('downloadButton').style.display = "none";
        return;
    }

    if (isBadWord(word)) {
        document.getElementById('translationOutput').textContent = "Mot inapproprié.";
        document.getElementById('imageOutput').style.display = "none";
        document.getElementById('prevImage').disabled = true;
        document.getElementById('nextImage').disabled = true;
        document.getElementById('downloadButton').style.display = "none";
        return;
    }

    fetch(`https://api.mymemory.translated.net/get?q=${word}&langpair=fr|en`)
        .then(response => response.json())
        .then(data => {
            const translation = data.responseData.translatedText;
            document.getElementById('translationOutput').textContent = translation;
            fetchImages(translation); // Recherche et affichage des images
        })
        .catch(error => {
            document.getElementById('translationOutput').textContent = "Erreur de traduction";
            document.getElementById('imageOutput').style.display = "none";
            document.getElementById('prevImage').disabled = true;
            document.getElementById('nextImage').disabled = true;
            document.getElementById('downloadButton').style.display = "none";
            console.error("Erreur:", error);
        });
}

function isBadWord(word) {
    return badWords.includes(word);
}

function fetchImages(translation) {
    const url = `https://api.pexels.com/v1/search?query=${translation}&per_page=10`; // Récupérer 10 images
    fetch(url, {
        headers: {
            Authorization: apiKey
        }
    })
    .then(response => response.json())
    .then(data => {
        if (data.photos.length > 0) {
            images = data.photos; // Stocker les images
            currentImageIndex = 0;
            displayImage(); // Afficher la première image
        } else {
            document.getElementById('imageOutput').style.display = "none";
            document.getElementById('prevImage').disabled = true;
            document.getElementById('nextImage').disabled = true;
            document.getElementById('downloadButton').style.display = "none";
        }
    })
    .catch(error => {
        console.error("Erreur lors de la récupération des images:", error);
        document.getElementById('imageOutput').style.display = "none";
        document.getElementById('prevImage').disabled = true;
        document.getElementById('nextImage').disabled = true;
        document.getElementById('downloadButton').style.display = "none";
    });
}

function displayImage() {
    const imageOutput = document.getElementById('imageOutput');
    imageOutput.src = images[currentImageIndex].src.medium;
    imageOutput.style.display = "block";
    document.getElementById('downloadButton').style.display = "block";
    
    document.getElementById('prevImage').disabled = currentImageIndex === 0;
    document.getElementById('nextImage').disabled = currentImageIndex === images.length - 1;
}

function prevImage() {
    if (currentImageIndex > 0) {
        currentImageIndex--;
        displayImage();
    }
}

function nextImage() {
    if (currentImageIndex < images.length - 1) {
        currentImageIndex++;
        displayImage();
    }
}

function downloadImage() {
    const link = document.createElement('a');
    link.href = images[currentImageIndex].src.original; // Lien vers l'image originale
    link.download = `image-${currentImageIndex + 1}.jpg`; // Nom du fichier
    link.click();
}
